include $(FSLCONFDIR)/default.mk

PROJNAME = copain
XFILES   = copain
LIBS     = -lfsl-miscplot -lfsl-miscpic -lfsl-newimage -lfsl-dpm \
           -lfsl-miscmaths -lfsl-utils  -lfsl-newran -lfsl-NewNifti \
           -lfsl-znz -lfsl-cprob  -lgdc -lgd -lpng

all: ${XFILES}

copain: copain.o copain_report.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}
