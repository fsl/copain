<!-- {{{ start -->

<HTML><HEAD><link REL="stylesheet" TYPE="text/css" href="fsl.css">
<TITLE>COPAIN - COnnectivity-based PArcellaIoN</TITLE></HEAD><BODY>
<hr><TABLE BORDER=0 WIDTH="100%"><TR>
<TD ALIGN=CENTER><H1>COPAIN</H1>

Connectivity-based parcellation using and Infinite Mixture Model<br><br>

<a href="#intro">Intro</a> &nbsp;&nbsp;-&nbsp;&nbsp;
<a href="#single">Running COPAIN - Single subject</a> &nbsp;&nbsp;-&nbsp;&nbsp;
<a href="#multiple">Running COPAIN - Multiple subjects</a> &nbsp;&nbsp;-&nbsp;&nbsp;

<TD ALIGN=RIGHT><a href="http://www.fmrib.ox.ac.uk/fsl/index.html"><IMG BORDER=0 SRC="images/fsl-logo.jpg"></a>
</TR></TABLE>

<!-- }}} -->
<!-- {{{ Introduction -->

<hr>


<a name="intro"></a><p><hr><H2>Introduction</H2>

<p>The motivation behind connectivity-based parcellation using tractography 
data is that regions that differ in their connections differ in their functions.
Hence, a segregation of brain regions according to their connections is likely 
to represent a functional segregation. COPAIN uses the output of a probtrackx analysis 
to segment a seed region into clusters that have different connectivity patterns.

<p>The main features of COPAIN are: (1) possibility of inferring the number of different 
subregions; (2) inclusion of spatial prior to get spatially smooth clusters; (3) multiple 
subjects mode that allows you to combine tractography results from different subjects.

<p>COPAIN assumes you have run tractography using probtrackx with at least one "classification target". 
Then it will use the output of probtrackx (the files called "seeds_to_[blah]" to perform the parcellation.

<p><i>Reference</i>:
<font size=-1><em>
<p>S. Jbabdi, M.W. Woolrich, T.E.J. Behrens
<BR>&nbsp;&nbsp;&nbsp;Multiple-subjects connectivity-based parcellation using hierarchical Dirichlet process mixture models.
<BR>&nbsp;&nbsp;&nbsp;NeuroImage, 44:373-384, 2009.
</em></font>

<p><b>Important notes:</b> <br>
COPAIN assumes you have run a tractography analysis using probtrackx and classification targets. For instructions 
on how to run probtrackx with classification targets see <a href="../fdt/doc/index.html">ProbtrackxDoc</a>. 
<br><br>We will assume in the following that you have 
N subjects, each one has an associated directory called <code>subject1</code>, <code>subject2</code>, etc. Within each subject's directory, we assume you have 
created a tractography results directory called <code>ptx_results</code> (same name for all subjects). 
<br><br>We will also assume that for each subject, the classification 
targets were the same (and in the same order) for all subjects (they do not need to be the same files or size, they just need to refer to the same brain area
 and have the same name for all subjects). We will assume the number of targets is T, and the target files are in the subject's directories, and called <code>subject1/target1.nii.gz</code>, <code>subject1/target2.nii.gz</code>, etc. And the same for the rest of the subjects. 
<br><br>Finally, we will assume that the seed masks are also in the subject's directories, and are called: <code>subject1/seed.nii.gz</code>, <code>subject2/seed.nii.gz</code>, etc.

<!-- }}} -->
<!-- {{{ single -->

<a name="single"></a><p><hr><H2>Running COPAIN - Single subject</H2>

Suppose we want to run COPAIN for subject1. The first thing to do is create a text file containing target names. For example:<br><br>

<code>
rm -f targets.txt<br>
for ((i=1;i<=T;i++));do <br>
&nbsp;&nbsp;echo target$i >> targets.txt <br>
done
</code>
<br><br>
Then the minimal command line to run COPAIN is:<br><br>

<code>
copain -d subject1/ptx_results -o copain -s subject1/seed -t targets.txt
</code>
<br><br>
This command will produce a single file: <code>subject1/ptx_results/copain_clusters.nii.gz</code>. This file contains the hard classification results, i.e. 
a volume file with non-zero values within the seed voxels, each value being an integer coding for the cluster index.
<br><br>
<b>Other options (also valid for multiple subjects):</b>
<br><br>
<ul>
<li>--report=&lt;directory&gt;</li>
Output a report directory with useful information about the analysis. Once COPAIN has finished, open <code>index.html</code> from within the specified directory.
<br><br>
<li>--spatial<br>
This option controls the spatial prior, i.e. the smoothness of the final clustering. It takes an argument that controls the weighting
 of the spatial prior relative to the data. It is set to zero by default (no smoothness constraint), and can take any value >0. There is no 
easy way of choosing this factor except playing around with different values and choosing the one that gives the best results :-(
<br><br>
<li>--targetstats<br>
Use this option to output the contribution of each target to the final clustering.
<br><br>
<li>--init<br>
Controls the initial clustering. Two options available: kmeans (default) and random.
<br><br>
<li>--nclasses<br>
Initial number of clusters.
<br><br>
<li>--dimred<br>
Perform PCA dimensionality reduction of the data prior to clustering. (concatenates the data across subjects prior to PCA in multiple subjects mode). Use this when you have many classification targets (e.g. > 10) and a large seed region.
<br><br>
<li>--finite<br>
Use this option to fix the number of clusters. The final number may be smaller than what you set for this option, but will not be bigger.
</ul>

<br><br>
For other options, simply type <code>copain</code> in a terminal window and press return.


<!-- }}} -->
<!-- {{{ multiple -->

<a name="multiple"></a><p><hr><H2>Running COPAIN - Multiple subjects</H2>

Running COPAIN in multiple subjects mode is as easy as running it in single subject mode. All you need to do is 
input a list of probtrackx directories instead of a single one, and a list of seeds instead of a single one. (The list of targets 
is supposed to be the same across subjects, so we only need a single list - you can use the same as created above for subject1).
<br><br>
Create a list of directories in a text file:<br><br>
<code>
rm -f ptx_dirs.txt <br>
for ((i=1;i<=N;i++));do <br>
&nbsp;&nbsp;echo subject${i}/ptx_results >> ptx_dirs.txt<br>
done
</code>
<br><br>
Then creat a list of seed files:<br><br>
<code>
rm -f ptx_seeds.txt <br>
for ((i=1;i<=N;i++));do <br>
&nbsp;&nbsp;echo subject${i}/seed.nii.gz >> ptx_seeds.txt<br>
done
</code>
<br><br>
Finally, run COPAIN using the following command (all the other options are also available in this mode):<br><br>
<code>
copain -d ptx_dirs.txt -o copain -s ptx_seeds.txt -t targets.txt
</code>
<br><br>
The output of this command will be a single file per subject: <code>subject1/ptx_results/copain_clusters.nii.gz</code>, <code>subject2/ptx_results/copain_clusters.nii.gz</code>, etc.

<!-- }}} -->
<!-- {{{ end -->

<p><HR><FONT SIZE=1>Copyright &copy; 2005-2009, University of
Oxford. Written by <A
HREF="http://www.fmrib.ox.ac.uk/~saad/index.html">S. Jbabdi</A>.</FONT>

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

</BODY></HTML>

<!-- }}} -->

