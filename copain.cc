/*  Copyright (C) 2008 University of Oxford  */
/*  COPAIN.CC - COnnectivity-based PArcellation using INfinite-mixture-models */

/* S. Jbabdi */

/*  CCOPYRIGHT  */

#undef __USE_BSD

#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <dirent.h>
#include <string>
#include <iostream>

/*
 * Work around potential conflict between
 * dirent.h and NewNifti/nifti1.h -
 * DT_UNKNOWN is a #define in the former,
 * and an enum value in the latter.
 */
#ifdef DT_UNKNOWN
#undef DT_UNKNOWN
#endif

#include "utils/options.h"
#include "armawrap/newmat.h"
#include "miscmaths/miscmaths.h"
#include "newimage/newimageall.h"
#include "copain_report.h"
#include "dpm/dpm_gibbs.h"
#include "dpm/hdpm_gibbs.h"




using namespace std;
using namespace Utilities;
using namespace NEWMAT;
using namespace MISCMATHS;
using namespace NEWIMAGE;
using namespace DPM;
using namespace COPAIN;


namespace COPAIN {

string padding(string in,unsigned int len){
  string out=in;
  while(out.length()<len){out="0"+out;}
  return out;
}
string padding(int in,unsigned int len){
  string out=num2str(in);
  while(out.length()<len){out="0"+out;}
  return out;
}


// options
string title="copain - COnnectivity-based PArcellation using INfinite-mixture-models";
string examples="copain -d <probtrackx-directory> -o <outdir> -s <seedname> -t <targetnames> [-k <nclust> --spatial=<beta>]";

Option<bool>   verbose(string("-v,--verbose"),false,
		       string("switch on diagnostic messages"),
		       false,no_argument);
Option<bool>   help(string("-h,--help"),false,
		    string("display this message"),
		    false,no_argument);
Option<string> ptxdir(string("-d,--ptxdir"),string(""),
		      string("probtrackx directory (or ascii textfile listing probtrackx directories for multi-subjects)"),
		      true,requires_argument);
Option<string> outdir(string("-o,--dir"),string("copain"),
			string("output directory - default='copain'"),
			false,requires_argument);
Option<float>  beta(string("--spatial"),0.0,
		    string("beta parameter for spatial prior (default=0.0)"),
		    false,requires_argument);
Option<string> seed(string("-s,--seedmask"),string(""),
		    string("seedmask (or list of seedmasks for multi-subjects). compulsory if spatial priors are used"),
		    true,requires_argument);
Option<string> init_class(string("--ic,--init"), "kmeans",
			  string("data labelling initialisation ['kmeans' (default) or 'random']"),
			  false, requires_argument);
Option<bool>   isfin(string("--finite"),false,
			string("if set, the model is a finite mixture model"),
			false,no_argument);
// Option<bool>   save(string("--savesamples"),false,
// 			string("if set, saves sample class parameters"),
// 			false,no_argument);
Option<string>  targets(string("-t,--targets"),"",
			string("list of target 'names' that have been used in tractography"),
			true,requires_argument);
Option<int>    numclass(string("-k,--nclasses"),5,
			string("initial number of classes - default=5"),
			false,requires_argument);
Option<int>    dimred(string("--dimred"),-1,
		      string("reduce data dimensions to this specified value"),
		      false,requires_argument);
Option<bool>   report(string("--report"),false,
		      string("create html page report (in outdir/report)"),
		      false,no_argument);
Option<bool>   forcedir(string("--forcedir"),false,
			string("overwrite existing output directory (i.e. do not add a '+')"),
			false,no_argument);
Option<int>    njumps(string("--nj,--njumps"),500,
		      string("number of iterations after burnin - default=500"),
		      false,requires_argument);
Option<int>    burnin(string("--bi,--burnin"),500,
		      string("number of iterations before sampling - default=500"),
		      false,requires_argument);
Option<int>    sampleevery(string("--se,--sampleevery"),10,
			   string("sampling frequency - default=10"),
			   false,requires_argument);


// Gets list of file names into a vector of string
bool getnames(string list,vector<string>& files){
  files.clear();
  ifstream fs(list.c_str());
  string tmp;
  if(fs){
    fs>>tmp;
    do{
      files.push_back(tmp);
      fs>>tmp;
    }while(!fs.eof());
  }
  if(files.size()==0){return false;}
  else{return true;}
}
void getdirnames(string dirlist,vector<string>& dirs){

  DIR *dp;

  dirs.clear();
  if((dp = opendir(dirlist.c_str())) == NULL){
    // might be a list of directories? browse this list
    ifstream fs(dirlist.c_str());
    string tmp;
    if(fs){
      fs>>tmp;
      do{
	dirs.push_back(tmp);
	fs>>tmp;
      }while(!fs.eof());
    }
    else{
      cerr<<dirlist<<" does not exist"<<endl;
      exit(0);
    }
  }
  else{
    // just one directory
    dirs.push_back(dirlist);
    return;
  }
}

// Gets all the files which name starts with seeds_to_* that are in a directory and stores their names in a vector of string
bool getfiles(string dir,vector<string>& files){
  DIR *dp;
  struct dirent *dirp;
  bool success;

  if((dp = opendir(dir.c_str())) == NULL){
    files.clear();
    while ((dirp = readdir(dp)) != NULL){
      if( string(dirp->d_name).substr(0,9) == "seeds_to_" ){
	if(verbose.value())
	  cout << "found: " << dir+"/"+string(dirp->d_name) << endl;
	files.push_back(dir+"/"+string(dirp->d_name));
      }
    }
    closedir(dp);
    success = (files.size()>0)?true:false;
  }
  else{
    success = false;
  }
  return success;
}
// given a list of directories, stores all the files starting with seeds_to_* into separate lists (one per directory)
void getfiles(string dirlist,vector< vector<string> >& allfiles,vector<string>& dirs){
  DIR *dp;
  struct dirent *dirp;
  vector<string> files;

  dirs.clear();
  if((dp = opendir(dirlist.c_str())) == NULL){
    // might be a list of directories? browse this list
    ifstream fs(dirlist.c_str());
    string tmp;
    if(fs){
      fs>>tmp;
      do{
	dirs.push_back(tmp);
	getfiles(tmp,files);
	if(!getfiles(tmp,files)){
	  cerr << "directory " << tmp << "doesn't contain ptx data" << endl;
	  exit(0);
	}
	fs>>tmp;
      }while(!fs.eof());
    }
    else{
      cerr<<dirlist<<" does not exist"<<endl;
      exit(0);
    }
  }
  else{
    // just one directory, check for seeds_to_* now
    dirs.push_back(dirlist);
    files.clear();
    while ((dirp = readdir(dp)) != NULL){
      if( string(dirp->d_name).substr(0,9) == "seeds_to_" ){
	if(verbose.value())
	  cout << "found: " << dirlist+"/"+string(dirp->d_name) << endl;
	files.push_back(dirlist+"/"+string(dirp->d_name));
      }
    }
    closedir(dp);
    allfiles.clear();
    allfiles.push_back(files);
    return;
  }
}

void getptxfiles(const vector<string>& dirs,const vector<string>& targetnames, vector< vector<string> >& allfiles){
  allfiles.resize(dirs.size());
  for(unsigned int i=0;i<dirs.size();i++){
    allfiles[i].clear();
    for(unsigned int j=0;j<targetnames.size();j++)
      allfiles[i].push_back(dirs[i]+"/seeds_to_"+targetnames[j]);
  }
}

// calculate histogram
ReturnMatrix hist(const ColumnVector& data,const int& nbins){
  float m = data.Minimum();
  float M = data.Maximum();
  float step = (M-m)/float(nbins);
  ColumnVector h(nbins);
  h=0;
  for(int i=1;i<=data.Nrows();i++){
    int bin = (int)(floor((data(i)-m)/step)+1);
    if(bin>=nbins)bin=nbins;
    if(bin<=0)bin=1;

    h(bin) += 1;
  }
  h.Release();
  return h;
}

ReturnMatrix hist(const ColumnVector& data,const ColumnVector& edges){
  int nbins,nedges;
  nbins  = edges.Nrows()-1;
  nedges = edges.Nrows();

  ColumnVector ret(nbins);
  ret = 0;
  for(int i=1;i<=data.Nrows();i++){
    double x = data(i);
    // eliminate boundary cases
    if( x<edges(1) || x>edges(nedges) )
      continue;
    // last bin
    if(x==edges(nedges)){
      ret(nbins)++;
      continue;
    }
    // find k such that edges(k) <= data(i) < edges(k+1)
    int k,kl=0,ku=nedges;
    while (1) {
      k = (ku + kl) / 2;
      if (k == kl) break;
      if (x < edges(k))
	ku = k;
      else
	kl = k;
    }
    ret(k)++;
  }
  ret.Release();
  return ret;
}
ReturnMatrix findedges(const float& m,const float& M,const int& nedges){
  ColumnVector ret(nedges);
  float step = (M-m) / float(nedges-1);
  ret(1) = m;
  for(int i=2;i<=nedges;i++)
    ret(i) = ret(1) + (i-1)*step;
  ret.Release();
  return ret;
}

ReturnMatrix cumsum(const ColumnVector& hist){
  ColumnVector ret(hist.Nrows());
  float s = 0;
  for(int i=1;i<=hist.Nrows();i++){
    s += hist(i);
    ret(i) = s;
  }
  ret.Release();
  return ret;
}

// histogram equalization
// matches histogram of source to the histogram of ref
// based on a decomposition of the data into 50 bins
void histeq(ColumnVector& src,const ColumnVector& ref){
  // set same min/max
  float ms = src.Minimum();
  float Ms = src.Maximum();
  float mr = ref.Minimum();
  float Mr = ref.Maximum();

  src = (src-ms)/(Ms-ms);
  src = src*(Mr-mr) + mr;

  // calculate histogram (set sum of both=1)
  int nbins = 100;
  ColumnVector hs,hr;
  ColumnVector edges;
  edges = findedges(mr,Mr,nbins+1);

  hs = COPAIN::hist(src,edges);
  hr = COPAIN::hist(ref,edges);

  hs /= hs.Sum();
  hr /= hr.Sum();

  // calculate cumulative histograms
  ColumnVector cumhs,cumhr;
  cumhs = cumsum(hs);
  cumhr = cumsum(hr);


  // for each source data point, do the following:
  // 1. find nearest edge
  // 2. interpolate bin count
  // 3. find nearest bin counts from target
  // 4. interpolate value from target bins
  for(int i=1;i<=src.Nrows();i++){
    // 1
    int k=1;
    for(int kk=1;kk<edges.Nrows();kk++){
      if(src(i)>=edges(kk) && src(i)<edges(kk+1)){
	k = kk;
	break;
      }
    }
    if(abs(src(i)-edges(k))>abs(src(i)-edges(k+1)))k++;

    // 2
    float interpcount;
    if(k==1) interpcount = cumhs(1);
    else if(k==edges.Nrows()) interpcount = cumhs(cumhs.Nrows());
    else
      interpcount = (cumhs(k)-cumhs(k-1))*(src(i)-(edges(k)+edges(k-1))/2)/((edges(k+1)-edges(k-1))/2) + cumhs(k-1);

    // 3
    k=1;
    if(interpcount<cumhr(1))k=1;
    else if(interpcount>=cumhr(cumhr.Nrows())) k=cumhr.Nrows();
    else{
      for(int kk=1;kk<cumhr.Nrows();kk++){
	if(interpcount>=cumhr(kk) && interpcount<cumhr(kk+1)){
	  k = kk;
	  break;
	}
      }
      if(abs(interpcount-cumhr(k))>abs(interpcount-cumhr(k+1)))k++;
    }

    // 4
    float interpval;
    if(k==1) interpval = src(i);
    else if(k==cumhr.Nrows()) interpval = src(i);
    else{
      if(cumhr(k)!=cumhr(k-1))
	interpval = (interpcount-cumhr(k-1))/(cumhr(k)-cumhr(k-1)) * (edges(k+1)-edges(k-1))/2 + (edges(k)+edges(k-1))/2;
      else
	interpval = (edges(k)+edges(k-1))/2;
    }

    src(i) = interpval;

  }

  return;

}

void histomatch(vector<Matrix>& data){
  unsigned int nsubj = data.size();
  int nbins = 100;

  for(int j=1;j<=data[0].Ncols();j++){

    vector<ColumnVector> histos(nsubj);
    for(unsigned int i=0;i<data.size();i++){
      histos[i] = COPAIN::hist(data[i].Column(j),nbins);
    }
    // determine best target for histogram matching
    ColumnVector scores(nsubj);
    unsigned int target=0;
    float minscore = 0;
    for(unsigned int i=0;i<nsubj;i++){
      scores(i+1) = 0;
      for(unsigned int j=i+1;j<nsubj;j++){
	scores(i+1) += (histos[i]-histos[j]).SumSquare();
      }
      if(scores(i+1)<minscore || i==0){
	minscore = scores(i+1);
	target   = i;
      }
    }
    // equalize histograms
    ColumnVector tmpcol(data[0].Nrows());
    for(unsigned int i=0;i<nsubj;i++){
      if(i!=target){
	tmpcol = data[i].Column(j);
	histeq(tmpcol,data[target].Column(j));
	data[i].Column(j) = tmpcol;
      }
    }
  }

}

// calculate pca decomposition using the covariance method
// X is the data nxd matrix with n observations (data points) and d variables (dimensions)
ReturnMatrix pcacov(Matrix& X,const int& dims,Matrix& projector){
  int n=X.Nrows();
  int d=X.Ncols();
  // de-mean
  ColumnVector Xmean(d);
  for(int j=1;j<=d;j++)
    Xmean(j) = X.Column(j).Sum()/n;
  for(int j=1;j<=d;j++){
    for(int i=1;i<=n;i++)
      X(i,j) -= Xmean(j);
  }
  // calculate covariance
  SymmetricMatrix C(d);
  if(d<n)
    C << (X.t()*X)/n;
  else
    C << X*X.t()/n;

  // eigenvalues
  Matrix V;
  DiagonalMatrix D;
  EigenValues(C,D,V);
  // select subset
   float cumsum=0,total=D.Trace();
   int dim=0;
   for(int i=D.Nrows();i>=1;i--){
     cumsum += D(i);
     if(dim<=dims){dim++;}
     else{break;}
   }
   if(verbose.value())
     cout << "variance explained after pca dimensionality reduction:"
	  << cumsum/total*100 << "%" << endl;
  Matrix v(V.Nrows(),dims);
  ColumnVector lam(dims);
  for(int j=1;j<=dims;j++){
    v.Column(j) = V.Column(V.Ncols()-j+1);
    lam(j)=D(V.Ncols()-j+1);
  }
  Matrix data(dims,n);
  if(!(d<n)){
    v = (X.t() * v);
    for(int j=1;j<=dims;j++)
      v.Column(j) /= sqrt(n*lam(j));
  }
  data = X*v;
  projector.ReSize(v.Nrows(),v.Ncols());
  projector = v;

  data.Release();
  return data;
}
ReturnMatrix dimreduce(Matrix& data,const int& numdims){
  if(verbose.value())
    cout << "------> PCA dimensionality reduction" << endl;
  //Matrix projector;
  Matrix newdat,projector;
  newdat=pcacov(data,numdims,projector);
  data.ReSize(newdat.Nrows(),newdat.Ncols());
  data=newdat;

  projector.Release();
  return projector;
}
void dimreduce(vector<Matrix>& data,Matrix& proj){
  bool do_dimred=true;
  int _dimred = dimred.value();
  if(_dimred<0 || _dimred>data[0].Ncols()){
    _dimred = data[0].Ncols();
    do_dimred=false;
  }

  if(!do_dimred)
    if(verbose.value())
      cout << "no dimensionality reduction" << endl;

  // reduce data
  if(verbose.value()) cout << "requested dimensions: " << _dimred << endl;
  if(_dimred>10){
    cout << endl;
    cout << "WARNING: We recommend using --dimred=10 or less" << endl << endl;
  }
  vector<int> nr(data.size());
  int nrtotal=0;
  int nc = data[0].Ncols();
  for(unsigned int i=0;i<data.size();i++){
    nr[i] = data[i].Nrows();
    nrtotal += nr[i];
  }

  Matrix concatdata(nrtotal,nc);
  int currow = 1;
  for(unsigned int i=0;i<data.size();i++){
    concatdata.SubMatrix(currow,currow+nr[i]-1,1,nc) = data[i];
    currow += nr[i];
  }
  proj=dimreduce(concatdata,_dimred);

  currow = 1;
  for(unsigned int i=0;i<data.size();i++){
    data[i] = concatdata.SubMatrix(currow,currow+nr[i]-1,1,_dimred);
    currow += nr[i];
  }


}

void read_data(const vector<string>& ptxfiles,const string& seedname,
	       Matrix& data,Matrix& coord,Matrix& adj){

  volume<float> seedmask;
  read_volume(seedmask,seedname);

  int npoints=0,curpoint;
  for(int z=0;z<seedmask.zsize();z++)
    for(int y=0;y<seedmask.ysize();y++)
      for(int x=0;x<seedmask.xsize();x++){
	if(seedmask(x,y,z)==0)continue;
	npoints++;
      }
  data.ReSize(npoints,ptxfiles.size());
  coord.ReSize(npoints,3);
  adj.ReSize(npoints,26);


  volume<float> tmpmask;
  for(unsigned int i=0;i<ptxfiles.size();i++){
    if(verbose.value())
      cout << "read s2t: " << ptxfiles[i] << endl;
    read_volume(tmpmask,ptxfiles[i]);

    curpoint=0;
    for(int z=0;z<seedmask.zsize();z++)
      for(int y=0;y<seedmask.ysize();y++)
	for(int x=0;x<seedmask.xsize();x++){
	  if(seedmask(x,y,z)==0)continue;
	  curpoint++;
	  data(curpoint,i+1) = tmpmask(x,y,z);
	}
  }

  curpoint=0;
  volume<float> lookup;
  lookup = seedmask;
  for(int z=0;z<seedmask.zsize();z++)
    for(int y=0;y<seedmask.ysize();y++)
      for(int x=0;x<seedmask.xsize();x++){
	if(seedmask(x,y,z)==0)continue;
	curpoint++;
	coord(curpoint,1) = x;
	coord(curpoint,2) = y;
	coord(curpoint,3) = z;
	lookup(x,y,z) = curpoint;
      }


  // compute adjacency matrix
  curpoint=0;
  for(int z=0;z<seedmask.zsize();z++)
    for(int y=0;y<seedmask.ysize();y++)
      for(int x=0;x<seedmask.xsize();x++){
	if(seedmask(x,y,z)==0)continue;
	curpoint++;
	short int curneighbour=0;
	for(int zz=-1;zz<=1;zz++)
	  for(int yy=-1;yy<=1;yy++)
	    for(int xx=-1;xx<=1;xx++){
	      if(abs(xx)+abs(yy)+abs(zz)==0)continue;
	      curneighbour++;
	      if(seedmask(x+xx,y+yy,z+zz)!=0)
		adj(curpoint,curneighbour) = lookup(x+xx,y+yy,z+zz);
	      else
		adj(curpoint,curneighbour) = -1;
	    }
      }
}


void preprocess(Matrix& data){

  // turn into proportions
  //for(int i=1;i<=data.Nrows();i++){
  //  if( sum(data.Row(i),2).AsScalar() > 0 )
  //   data.Row(i) = data.Row(i) / sum(data.Row(i),2).AsScalar();
  //}


  // log transform
  // data range is [0,1]
  // turn it into [0,a] and apply Fisher transform
  // float a = .9;
   for(int i=1;i<=data.Nrows();i++){
     for(int j=1;j<=data.Ncols();j++){
       data(i,j) = std::log((1+data(i,j)));
//       //data(i,j) = .5*std::log( (1+2*a*data(i,j)-a)/(1-2*a*data(i,j)+a));
//       data(i,j) = data(i,j);//std::log(1+data(i,j)*5000.0);
     }
   }

  // add noise
  float addterm;
  for(int i=1;i<=data.Nrows();i++){
    for(int j=1;j<=data.Ncols();j++){
      addterm=0.05*normrnd(1,1).AsScalar();
      data(i,j) = data(i,j) + addterm;
    }
  }



}


void save_clusters(const ColumnVector& z,const Matrix& coord,const string& filename,const string& dir,const int& sub=0){
  volume<int> omask;
  read_volume(omask,filename);

  omask=0;
  int minclust=z.Minimum();
  for(int i=1;i<=coord.Nrows();i++){
    omask(coord(i,1),coord(i,2),coord(i,3)) = int(z(i)) - minclust + 1;
  }

  omask.setDisplayMaximumMinimum(omask.max(),omask.min());
  save_volume(omask,dir+"/subject"+num2str(padding(sub,3))+"_clusters");

}

void save_fuzzy(const Matrix& fuzzy_z,const Matrix& coord,const string& filename,const string& dir,const int& sub=0){
  volume<float> omask;
  read_volume(omask,filename);

  omask=0;

  for(int t=1;t<=fuzzy_z.Ncols();t++){
    for(int i=1;i<=coord.Nrows();i++)
      omask(coord(i,1),coord(i,2),coord(i,3)) = fuzzy_z(i,t);
    omask.setDisplayMaximumMinimum(0,1);
    save_volume(omask,dir+"/subject"+num2str(padding(sub,3))+"_weights"+num2str(padding(t,2)));

  }

}

// void output_fstats(const Matrix& F,const vector<string>& names){
//   cout << endl << endl << "------------ TARGET STATS ------------" << endl;
//   cout << " (sumsquared mean diff) |  (sumsquared mean diff)/(sum variances)" << endl << endl;
//   for(int i=1;i<=F.Nrows();i++){
//     cout << names[i-1] << "  -->\t";
//     for(int j=1;j<=F.Ncols();j++){
//       cout << F(i,j) << "\t";
//     }
//     cout << endl;
//   }
//   cout << endl << "--------------------------------------" << endl << endl;
// }
}


int main (int argc, char *argv[]){

  // initialise random seed
  //srand(time(NULL));
  srand(1);

  // command line options
  OptionParser options(title,examples);
  options.add(verbose);
  options.add(help);
  options.add(ptxdir);
  options.add(outdir);
  options.add(COPAIN::beta);
  options.add(seed);
  options.add(init_class);
  options.add(isfin);
//   options.add(save);
  options.add(targets);
  options.add(numclass);
  options.add(dimred);
  options.add(report);
  options.add(forcedir);
  options.add(njumps);
  options.add(burnin);
  options.add(sampleevery);

  options.parse_command_line(argc,argv);

  if ( (help.value()) || (!options.check_compulsory_arguments(true)) ){
      options.usage();
      exit(EXIT_FAILURE);
  }
  if( COPAIN::beta.value() != 0 && seed.value()==""){
    cerr << endl
	 << "Please provide a seed mask (or list of seeds) if spatial priors turned on."
	 << endl << endl;
    exit(EXIT_FAILURE);
  }

  if(verbose.value())
    cout << "------> Check input files and directories" << endl;

  // ColumnVector y1,y2;
//   y1 = normrnd(1000,1);
//   write_ascii_matrix(y1,"y1");
//   y2 = unifrnd(1000,1);
//   write_ascii_matrix(y2,"y2");

//   histeq(y1,y2);
//   write_ascii_matrix(y1,"y3");

//   exit(0);


  // read probtrackx directories
  vector< vector<string> > ptxfiles;
  vector<string> dirs;
  vector<string> targetnames;

  getdirnames(ptxdir.value(),dirs);

  getnames(targets.value(),targetnames);

  getptxfiles(dirs,targetnames,ptxfiles);

  // read seed masks
  vector<string> seednames;
  if(seed.value() != ""){
    if(fsl_imageexists(seed.value()))
      seednames.push_back(seed.value());
    else{
      getnames(seed.value(),seednames);
    }
  }


  // check intra-inter subject compatibilities
  if( (seed.value()!="") && (seednames.size()!=ptxfiles.size()) ){
    cerr << "number of seed files incompatible with number of ptx directories" << endl;
    exit(0);
  }
  unsigned nsubj = dirs.size();
  unsigned ntargets = ptxfiles[0].size();
  for(unsigned int i=0;i<ptxfiles.size();i++){
    if(ptxfiles[i].size()!=ntargets){
      cerr << "one or more subjects has a different number of seeds_to_* files" << endl;
      exit(0);
    }
  }

  // create data for classification
  vector<Matrix> data(nsubj);
  vector<Matrix> coord(nsubj);
  vector<Matrix> adj(nsubj);
  Matrix         proj;


  // read data
  for(unsigned int i=0;i<nsubj;i++){
    if(verbose.value())
      cout << "------> reading subject " <<  i+1 << " data" << endl;
    read_data(ptxfiles[i],seednames[i],data[i],coord[i],adj[i]);
  }

  // create output directory
  string dirname=outdir.value();
  if(!forcedir.value()){
    DIR *dp;
    while( (dp=opendir(dirname.c_str())) != NULL){
      dirname = dirname+"+";
    }
  }
  else{
    rmdir(dirname.c_str());
  }
  mkdir(dirname.c_str(),0777);


  // initialise report class
  CopainReport cr(seednames,dirs,targetnames);
  if(report.value()){
    mkdir((dirname+"/report").c_str(),0777);
    cr.set_rawdata(data);
  }


  // preprocessing
  for(unsigned int i=0;i<nsubj;i++){
    if(verbose.value())
      cout << "------> preprocessing subject " << i+1 << " data" << endl;
    preprocess(data[i]);
  }

  // reduce dimensionality
  dimreduce(data,proj);

  if(nsubj>1){
    // match histograms
    histomatch(data);
  }
  //for(unsigned int i=0;i<data.size();i++)
  //write_ascii_matrix(data[i],"data"+num2str(i));

  //exit(0);


  // for html report
  if(report.value()){
    cr.set_data(data);
  }

  // create dpm class and run the algorithm
  // single subject
  if(data.size()==1){
    if(verbose.value())
      cout << "------> Single subject DPM" << endl;

    GWDPM_GibbsSampler gs(data[0],njumps.value(),burnin.value(),sampleevery.value());
    gs.set_infinite(!isfin.value());
    if(COPAIN::beta.value()>0)
      gs.add_spatial_prior(adj[0]);
    gs.set_spcparam(COPAIN::beta.value());
    gs.set_projector(proj);

    if(verbose.value())
      cout << ".....init......";
    gs.init(numclass.value(),init_class.value());
    if(verbose.value())
      cout << "run......"<<endl;
    gs.run();
    if(verbose.value())
      cout << gs << endl;

    cout<<endl;
    if(verbose.value())
      cout << "------> postprocessing" << endl;

    if(verbose.value())
      cout << "calculate MAP posteriors" << endl;
    gs.calc_posterior_stats();

    if(verbose.value())
      cout << "------> saving clustering" << endl;
    save_clusters(gs.get_map_z(),coord[0],seednames[0],dirname);
    save_fuzzy(gs.get_map_fuzzy_z(),coord[0],seednames[0],dirname);

    if(report.value()){
      if(verbose.value())
	cout << "create web report" << endl;

      cr.set_stats(gs);
      cr.set_coord(coord);
      cr.create_report(dirname+"/report");
    }
  }
  // multiple subjects
  else{
    if(verbose.value())
      cout << "------> Multiple subjects DPM" << endl;

    GWHDPM_GibbsSampler gs(data,njumps.value(),burnin.value(),sampleevery.value());
    gs.set_infinite(!isfin.value());
    if(COPAIN::beta.value()>0)
      gs.add_spatial_prior(adj);
    gs.set_spcparam(COPAIN::beta.value());
    gs.set_projector(proj);

    if(verbose.value())
      cout << ".....init......";
    gs.init(numclass.value(),init_class.value());
    if(verbose.value())
      cout << "run......"<<endl;
    gs.run();
    //if(save.value()){
      //if(verbose.value())
    //cout << "save......";
    //gs.save(dirs[i] + "/" basename2.value());
    //}

    if(verbose.value())
      cout << "calculate MAP posteriors" << endl;
    gs.calc_posterior_stats();
    vector<Matrix> fuzzy = gs.get_map_fuzzy_z();

  if(verbose.value())
    cout << "------> saving clustering" << endl;
    for(unsigned int i=0;i<data.size();i++){
      if(verbose.value())
	cout << i+1 << " ";
      save_clusters(gs.get_map_z(i),coord[i],seednames[i],dirname,i);
      save_fuzzy(fuzzy[i],coord[i],seednames[i],dirname,i);
    }
    if(verbose.value())
      cout << endl;


    if(report.value()){
      if(verbose.value())
	cout << "create web report" << endl;

      cr.set_stats(gs);
      cr.set_coord(coord);
      cr.create_report(dirname+"/report");
    }
  }


  if(verbose.value())
    cout << "------> Done." << endl;


  return 0;

}
