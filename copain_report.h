#if !defined(copain_report_h)
#define copain_report_h

#include "armawrap/newmat.h"

#include "libvis/miscplot.h"
#include "libvis/miscpic.h"
#include "dpm/dpm_gibbs.h"
#include "dpm/hdpm_gibbs.h"

#include "newimage/newimageall.h"

// this class contains information about posterior distributions
// should only know about means, variances, and weights along each dimension
namespace COPAIN {
class Stats {
public:
  Stats () {}



  void set_stats(const DPM::GWDPM_GibbsSampler& gs){
    std::vector<NEWMAT::SymmetricMatrix> s;
    std::vector<NEWMAT::SymmetricMatrix> s_pca;

    means_pca   = gs.get_map_means();
    s_pca       = gs.get_map_variances();

    means = means_pca;
    s     = s_pca;
    gs.project_classes(means,s);
    projector = gs.get_projector();

    weights = gs.get_map_proportions();
    z       = gs.get_map_z();
    fuzzy_z = gs.get_map_fuzzy_z();

    nclasses = (int)s.size();

    zs.clear();zs.push_back(z);
    fuzzy_zs.clear();fuzzy_zs.push_back(fuzzy_z);

    variances.resize(s.size());
    NEWMAT::ColumnVector w(s.size());
    for(unsigned int cl=0;cl<s.size();cl++){
      variances[cl].ReSize(s[cl].Nrows());
      for(int tgt=1;tgt<=s[cl].Nrows();tgt++)
	variances[cl](tgt) = s[cl](tgt,tgt);
      w(cl+1) =  weights[cl];

    }

    variances_pca.resize(s_pca.size());
    for(unsigned int cl=0;cl<s_pca.size();cl++){
      variances_pca[cl].ReSize(s_pca[cl].Nrows());
      for(int tgt=1;tgt<=s_pca[cl].Nrows();tgt++)
	variances_pca[cl](tgt) = s_pca[cl](tgt,tgt);
    }
    compatible_subjects=true;
  }
  void set_stats(const DPM::GWHDPM_GibbsSampler& gs){
    std::vector<NEWMAT::SymmetricMatrix> s;
    std::vector<NEWMAT::SymmetricMatrix> s_pca;

    means_pca   = gs.get_map_means();
    s_pca       = gs.get_map_variances();

    means = means_pca;
    s     = s_pca;
    gs.project_classes(means,s);
    projector = gs.get_projector();

    weights  = gs.get_map_proportions();
    zs       = gs.get_map_z();
    fuzzy_zs = gs.get_map_fuzzy_z();

    nclasses = (int)s.size();

    // calculate group maps
    compatible_subjects = true;
    int npts = zs[0].Nrows();
    for(unsigned int i=0;i<zs.size();i++)
      if(zs[i].Nrows()!=npts){compatible_subjects=false;break;}

    if(compatible_subjects){
      z.ReSize(npts);z=0;
      fuzzy_z.ReSize(npts,nclasses);
      fuzzy_z=0;
      for(int i=1;i<=npts;i++){
	NEWMAT::ColumnVector cnt(nclasses);
	cnt=0;
	for(unsigned int sub=0;sub<zs.size();sub++){
	  cnt((int)zs[sub](i)+1)++;
	  fuzzy_z.Row(i) += fuzzy_zs[sub].Row(i);
	}
	int ind;
	float junk;
	junk = cnt.Maximum1(ind);
	z(i) = ind-1;
	fuzzy_z.Row(i) /= float(zs.size());
      }
    }

    variances.resize(s.size());

    NEWMAT::ColumnVector w(s.size());
    for(unsigned int cl=0;cl<s.size();cl++){
      variances[cl].ReSize(s[cl].Nrows());
      for(int tgt=1;tgt<=s[cl].Nrows();tgt++)
    	variances[cl](tgt) = s[cl](tgt,tgt);
      w(cl+1) =  weights[cl];

    }

    variances_pca.resize(s_pca.size());
    for(unsigned int cl=0;cl<s_pca.size();cl++){
      variances_pca[cl].ReSize(s_pca[cl].Nrows());
      for(int tgt=1;tgt<=s_pca[cl].Nrows();tgt++)
	variances_pca[cl](tgt) = s_pca[cl](tgt,tgt);
    }
  }


  float get_mean(const int& cl,const int& tgt)const{return means[cl](tgt);}
  float get_var(const int& cl,const int& tgt)const{return variances[cl](tgt);}
  float get_weight(const int& cl)const{return weights[cl];}


  NEWMAT::ReturnMatrix get_fstats(const int& r1,const int& r2){
    NEWMAT::ColumnVector f(means[0].Nrows());

    for(int k=1;k<=means[0].Nrows();k++){
      float mu  = std::abs(means[r1](k) - means[r2](k));
      float sig = std::sqrt(variances[r1](k) + variances[r2](k));

      f(k) = .5*(1+MISCMATHS::erf(-mu/(std::sqrt(2)*sig)));
    }

    f.Release();
    return f;

  }

  NEWMAT::ReturnMatrix get_means(const int& tgt)const{
    NEWMAT::ColumnVector ret(nclasses);
    for(int cl=0;cl<nclasses;cl++){
      ret(cl+1) = means[cl](tgt);
    }
    ret.Release();
    return ret;
  }

 NEWMAT::ReturnMatrix get_vars(const int& tgt)const{
    NEWMAT::ColumnVector ret(nclasses);
    for(int cl=0;cl<nclasses;cl++){
      ret(cl+1) = variances[cl](tgt);
    }
    ret.Release();
    return ret;
  }

  NEWMAT::ReturnMatrix get_means_pca(const int& j)const{
    NEWMAT::ColumnVector ret(nclasses);
    for(int cl=0;cl<nclasses;cl++){
      ret(cl+1) = means_pca[cl](j);
    }
    ret.Release();
    return ret;
  }

 NEWMAT::ReturnMatrix get_vars_pca(const int& j)const{
    NEWMAT::ColumnVector ret(nclasses);
    for(int cl=0;cl<nclasses;cl++){
      ret(cl+1) = variances_pca[cl](j);
    }
    ret.Release();
    return ret;
  }

 NEWMAT::ReturnMatrix get_weights()const{
    NEWMAT::ColumnVector ret(nclasses);
    for(int cl=0;cl<nclasses;cl++){
      ret(cl+1) = weights[cl];
    }
    ret.Release();
    return ret;
  }
  const std::vector<NEWMAT::ColumnVector>& get_zs()const{return zs;}
  const std::vector<NEWMAT::Matrix>& get_fuzzy_zs()const{return fuzzy_zs;}
  const NEWMAT::ColumnVector& get_z()const{return z;}
  const NEWMAT::Matrix& get_fuzzy_z()const{return fuzzy_z;}

  const int& get_nclasses()const{return nclasses;}

  bool compatible()const{return compatible_subjects;}

  void get_classvals(const int& cl,int& classnd,int& classns,float& w,const int sub=-1)const{
    classnd=0;
    classns=0;
    w=0.0;
    float out_w=0.0;
    if(sub==-1){
      for(unsigned int s=0;s<zs.size();s++){
	bool visited=false;
	for(int i=1;i<=zs[s].Nrows();i++){
	  if(zs[s](i)==(cl-1)){
	    classnd++;
	    if(!visited){classns++;visited=true;}
	    w += fuzzy_zs[s](i,cl);
	  }
	  else{
	    out_w += fuzzy_zs[s](i,cl);
	  }
	}
      }
    }
    else{
      for(int i=1;i<=zs[sub].Nrows();i++){
	if(zs[sub](i)==(cl-1)){
	  classnd++;
	  w += fuzzy_zs[sub](i,cl);
	}
	else{
	  out_w += fuzzy_zs[sub](i,cl);
	}
      }
    }
    w /= (w+out_w);

  }

  void get_coord(const int& cl,const std::vector<NEWMAT::Matrix>& coord,NEWMAT::ColumnVector& cog,NEWMAT::ColumnVector& peak,const int sub=-1){
    cog.ReSize(3);
    peak.ReSize(3);
    cog=0.0;

    float nz=0.0;float maxw=-1,w;
    if(sub==-1){
      for(int i=1;i<=z.Nrows();i++)
	if(z(i)==(cl-1)){
	  nz ++;
	  cog += coord[0].Row(i).t();
	  w = fuzzy_z(i,cl);
	  if(maxw<0 || w>maxw){
	    maxw=w;
	    peak = coord[0].Row(i).t();
	  }
	}
      cog /= nz;
    }
    else{
      for(int i=1;i<=zs[sub].Nrows();i++)
	if(zs[sub](i)==(cl-1)){
	  nz ++;
	  cog += coord[sub].Row(i).t();
	  w = fuzzy_zs[sub](i,cl);
	  if(maxw<0 || w>maxw){
	    maxw=w;
	    peak = coord[sub].Row(i).t();
	  }
	}
      cog /= nz;

    }


  }

  NEWMAT::Matrix get_projector()const{return projector;}

private:
  std::vector<NEWMAT::ColumnVector> means;
  std::vector<NEWMAT::ColumnVector> variances;

  std::vector<NEWMAT::ColumnVector> means_pca;
  std::vector<NEWMAT::ColumnVector> variances_pca;

  std::vector<float> weights;
  int nclasses;
  std::vector<NEWMAT::ColumnVector> zs; // all subjects
  std::vector<NEWMAT::Matrix> fuzzy_zs;
  NEWMAT::ColumnVector z; // group or one subject
  NEWMAT::Matrix fuzzy_z;

  NEWMAT::Matrix projector;

  bool compatible_subjects;

};

class CopainReport {
public:
  CopainReport(const std::vector<std::string>& iseednames,
	       const std::vector<std::string>& isubjnames,
	       const std::vector<std::string>& itargetnames):
    seednames(iseednames),subjnames(isubjnames),targetnames(itargetnames){}

  void set_stats(const DPM::GWDPM_GibbsSampler& gs){stats.set_stats(gs);}
  void set_stats(const DPM::GWHDPM_GibbsSampler& gs){stats.set_stats(gs);}

  void set_data(const std::vector<NEWMAT::Matrix>& in){data=in;}
  void set_rawdata(const std::vector<NEWMAT::Matrix>& in){rawdata=in;}

  void project_data(const DPM::GWDPM_GibbsSampler& gs){
    NEWMAT::Matrix P = gs.get_projector();
    for(unsigned int i=0;i<data.size();i++)
      data[i] = (P*data[i].t()).t();
  }
  void project_data(){
    NEWMAT::Matrix P = stats.get_projector();
    for(unsigned int i=0;i<data.size();i++)
      data[i] = (P*data[i].t()).t();
  }



  void set_mask(const NEWIMAGE::volume<float>& m){mask=m;}
  void set_coord(const std::vector<NEWMAT::Matrix>& c){coord=c;}

  void create_report(const std::string& dirname);
  void create_html_table();
  void create_html_table(const int& subjID);

  void plot_hist(std::ofstream& htmlfile,const std::string& dirname,const std::string& basename,
		 const NEWMAT::Matrix& dat,const std::string& xlabel);


  void plot_fit(std::ofstream& htmlfile,const std::string& dirname,const std::string& basename,
		const NEWMAT::Matrix& dat,const NEWMAT::ColumnVector& m,const NEWMAT::ColumnVector& v,const NEWMAT::ColumnVector& w,
		const std::string& xlabel);

  void plot_table_clusters(std::ofstream& htmlfile,const int& sub=-1);
  void plot_table_fstats(std::ofstream& htmlfile);
  void save_coord(const std::string& dirname);


private:
  Stats stats;

  std::vector<NEWMAT::Matrix> data;
  std::vector<NEWMAT::Matrix> rawdata;

  NEWIMAGE::volume<float> mask;
  std::vector<NEWMAT::Matrix> coord;

  const std::vector<std::string>& seednames;
  const std::vector<std::string>& subjnames;
  const std::vector<std::string>& targetnames;

};

}
#endif
