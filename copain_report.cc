#include <string>
#include <iostream>

#include "armawrap/newmat.h"
#include "miscmaths/miscmaths.h"
#include "libvis/miscplot.h"

#include "copain_report.h"


using namespace std;
using namespace NEWMAT;
using namespace MISCMATHS;
using namespace MISCPLOT;

namespace COPAIN {

void CopainReport::plot_hist(ofstream& htmlfile,const string& dirname,const string& basename,
	       const Matrix& dat,const string& xlabel){

    miscplot histplot;
    histplot.add_xlabel(xlabel);
    histplot.set_xysize(300,150);
    histplot.histogram(dat.t(),dirname+"/"+basename,"");
    htmlfile << "<img border=0 src=\"" <<  basename << ".png\">"<< endl;

}


void CopainReport::plot_fit(ofstream& htmlfile,const string& dirname,const string& basename,
	      const Matrix& dat,const ColumnVector& m,const ColumnVector& v,const ColumnVector& w,
	      const string& xlabel){
  miscplot gmmplot;
  gmmplot.add_xlabel(xlabel);
  gmmplot.set_xysize(300,150);
  gmmplot.gmmfit(dat.t(),m,v,w,dirname+"/"+basename,"");
  htmlfile << "<img border=0 src=\"" << basename << ".png\">"<< endl;

}

void CopainReport::save_coord(const string& dirname){

  Matrix COG(subjnames.size(),3);
  Matrix PEAK(subjnames.size(),3);

  for(int i=1;i<=stats.get_nclasses();i++){
    for(unsigned int s=0;s<subjnames.size();s++){
      ColumnVector cog(3),peak(3);
      stats.get_coord(i,coord,cog,peak,s);
      COG.Row(s+1)  = cog.t();
      PEAK.Row(s+1) = peak.t();
    }
    write_ascii_matrix(COG,dirname+"/"+"COG"+num2str(i)+".txt");
    write_ascii_matrix(PEAK,dirname+"/"+"MAX"+num2str(i)+".txt");

  }

}

void CopainReport::plot_table_clusters(ofstream& htmlfile,const int& sub){

   htmlfile << "<hr>" << endl;
   htmlfile << "<h3>Clustering summary</h3><br>" << endl;

   htmlfile << "In multiple subjects mode, the #voxels per cluster is taken from the MAP across subjects<br>"<<endl;
   htmlfile << "The voxel coordinates are taken from subject #1. For individual coordinates, please go to individual report pages (links at the top of this page)<br>"<<endl;
   htmlfile << "When subjects do not have the same seed (tested according to the number of voxels), we output a cross (X) at a group level.<br>" << endl;
   htmlfile << "The last columns show connectivity strength of each cluster to each target. These are taken from the rawdata, scaled for each voxel according to its class membership, then transformed into proportions (i.e. the values across targets sum to one)<br>"<<endl;

   htmlfile << "<TABLE border=\"1\" frame=\"box\" rules=\"all\" summary=\"Copain summary\"cellspacing=\"5\" cellpadding=\"5%\">  <CAPTION>Copain summary</CAPTION>" << endl;

   // do column headings
   htmlfile << "<THEAD align=\"center\"><TR><TD>";
   htmlfile << "<TD><em> # voxels</em>";
   if(sub<0)
     htmlfile << "<TD><em> # subjects </em>";
   htmlfile << "<TD><em> COG - x </em>";
   htmlfile << "<TD><em> COG - y </em>";
   htmlfile << "<TD><em> COG - z </em>";
   htmlfile << "<TD><em> MAX - x </em>";
   htmlfile << "<TD><em> MAX - y </em>";
   htmlfile << "<TD><em> MAX - z </em>";
   for(unsigned int i=0; i<targetnames.size(); i++)
     htmlfile << "<TD><em>"<<targetnames[i]<<"</em>";


   htmlfile << "</THEAD><TBODY align=\"center\">" << endl;

   // do table
   for(int r=1; r<=stats.get_nclasses(); r++){
     htmlfile << "<TR>" << " <TD><em>cluster" << r << "</em>" << endl;

     int val,ns;
     float weight;
     stats.get_classvals(r,val,ns,weight,sub);
     htmlfile << "<TD> "  << val << endl;
     if(sub<0)
       htmlfile << "<TD> " << ns << endl;

     ColumnVector cog(3),peak(3);
     stats.get_coord(r,coord,cog,peak,sub);

     if(!stats.compatible() && sub<0){
       htmlfile << "<TD> X <TD> X <TD> X" << endl;
       htmlfile << "<TD> X <TD> X <TD> X" << endl;
     }
     else{
       htmlfile << "<TD> " << cog(1) << "<TD> " << cog(2) << "<TD> " << cog(3) << endl;
       htmlfile << "<TD> " << peak(1) << "<TD> " << peak(2) << "<TD> " << peak(3) << endl;
     }

     // raw connectivity scores to different targets

     ColumnVector vals(targetnames.size());
     vals = 0;
     for(unsigned int i=0;i<targetnames.size();i++){
       float sc=0;
       if(sub<0){

	 for(unsigned int s=0;s<subjnames.size();s++){
	   for(int pt=1;pt<=coord[s].Nrows();pt++){
	     sc  += stats.get_fuzzy_zs()[s](pt,r);
	     vals(i+1) += stats.get_fuzzy_zs()[s](pt,r) * rawdata[s](pt,i+1);
	   }
	 }
	 vals(i+1) /= sc;
       }
       else{
	 for(int pt=1;pt<=coord[sub].Nrows();pt++){
	   sc  += stats.get_fuzzy_zs()[sub](pt,r);
	   vals(i+1) += stats.get_fuzzy_zs()[sub](pt,r) * rawdata[sub](pt,i+1);
	 }
	 vals(i+1) /= sc;
       }
     }
     vals /= vals.Sum();
     int indmax;
     vals.Maximum1(indmax);

     for(unsigned int i=0;i<targetnames.size();i++){
       if((int)(i+1)!=indmax)
	 htmlfile << "<TD> " << vals(i+1);
       else
	 htmlfile << "<TD> <b>" << vals(i+1) << "</b>";

     }

     htmlfile << endl;
   }

   htmlfile << "</TBODY></TABLE>" << endl;




}



void CopainReport::plot_table_fstats(ofstream& htmlfile){
  htmlfile << "<hr>" << endl;
  htmlfile << "<h3>Clustering separability</h3><br>" << endl;

  htmlfile << "<TABLE border=\"1\" frame=\"box\" rules=\"all\" summary=\"Cluster separation power\"cellspacing=\"5\" cellpadding=\"5%\">" ;
  htmlfile << "<CAPTION>Cluster-pairs separability for each target</CAPTION>" << endl;

  // do column headings
  htmlfile << "<THEAD align=\"center\"><TR><TD>";

  for(unsigned int i=0;i<targetnames.size();i++)
    htmlfile << "<TD><em>" << targetnames[i] << "</em>";


  htmlfile << "</THEAD><TBODY align=\"center\">" << endl;

  // do table
  for(int r1=1; r1<=stats.get_nclasses(); r1++){
    for(int r2=r1+1;r2<=stats.get_nclasses();r2++){
      htmlfile << "<TR>" << " <TD><em>" << r1 << "-" << r2 << "</em>" << endl;

      ColumnVector f = stats.get_fstats(r1-1,r2-1);

      for(int i=1;i<=f.Nrows();i++){
	if(f(i)>0.05)
	  htmlfile << "<TD> " << f(i);
	else
	  htmlfile << "<TD><b>" << f(i) << "</b>";
      }
    }
    htmlfile << endl;
  }

  htmlfile << "</TBODY></TABLE>" << endl;



}

void CopainReport::create_report(const string& dirname){

  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  // setup html report file
  ///////////////////////////////////////////////////////////////////////////////////////////////////////

  string htmlfilename = dirname + "/index.html";
  ofstream htmlfile(htmlfilename.c_str());


  htmlfile << "<html> " << endl
           << "<title> COPAIN HTML REPORT </title>"
	   << "<body background=\"file:" << getenv("FSLDIR")
	   << "/doc/images/fsl-bg.jpg\">" << endl
	   << "<hr><centre><h1>COPAIN RESULTS<br><br><br><hr>" << endl;

  if(subjnames.size()>1){
    htmlfile << "Individual results<br><br>" << endl;
    htmlfile << "<font size=3>" << endl;
    for(unsigned int i=0;i<subjnames.size();i++)
      htmlfile << "<a href=\"copain_report_" + num2str(i) + ".html\" size=2>" + subjnames[i] + "</a><br>"
	       <<endl;
    htmlfile << "</font>" << endl;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  // plot histograms of the raw data
  ///////////////////////////////////////////////////////////////////////////////////////////////////////

  htmlfile << "<hr>" << endl;
  htmlfile << "<h3>Raw Data</h3><br><br>" << endl;
  // plot histograms for raw data (one image per target)
  int npts=0;
  for(unsigned int i=0;i<data.size();i++)
    npts += data[i].Nrows();

  Matrix dat(npts,1);
  int currow;
  for(unsigned int t=0;t<targetnames.size();t++){
    currow=1;
    for(unsigned int s=0;s<subjnames.size();s++){
      dat.SubMatrix(currow,currow+rawdata[s].Nrows()-1,1,1) = rawdata[s].SubMatrix(1,rawdata[s].Nrows(),t+1,t+1);
      currow += rawdata[s].Nrows();
    }
    plot_hist(htmlfile,dirname,"rawdata"+num2str(t+1)+"_hist",dat,targetnames[t]);

  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  // plot fits to the PCA processed data
  ///////////////////////////////////////////////////////////////////////////////////////////////////////

  htmlfile << "<hr>" << endl;
  htmlfile << "<hr>" << endl;
  htmlfile << "<h3>GMM FIT to PCA processed data</h3><br><br>" << endl;


  for(int t=0;t<data[0].Ncols();t++){
    currow=1;
    for(unsigned int s=0;s<subjnames.size();s++){
      dat.SubMatrix(currow,currow+rawdata[s].Nrows()-1,1,1) = data[s].SubMatrix(1,data[s].Nrows(),t+1,t+1);
      currow += rawdata[s].Nrows();
    }
    plot_fit(htmlfile,dirname,"data"+num2str(t+1)+"_gmm_pca",
	     dat,stats.get_means_pca(t+1),stats.get_vars_pca(t+1),stats.get_weights(),
	     "");
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  // plot fits in data space
  ///////////////////////////////////////////////////////////////////////////////////////////////////////

  htmlfile << "<h3>GMM FIT to preprocessed data</h3><br><br>" << endl;
  htmlfile << "Preprocessing consists of log transforming the data and adding gaussian noise in each dimension (std=0.05) <br>" << endl;
  htmlfile << "When there is more than one subject, histogram matching is done between subjects in each dimension (i.e. for each target) <br><br>" << endl;

  project_data();
  for(unsigned int t=0;t<targetnames.size();t++){
    currow=1;
    for(unsigned int s=0;s<subjnames.size();s++){
      dat.SubMatrix(currow,currow+rawdata[s].Nrows()-1,1,1) = data[s].SubMatrix(1,data[s].Nrows(),t+1,t+1);
      currow += rawdata[s].Nrows();
    }

    plot_fit(htmlfile,dirname,"data"+num2str(t+1)+"_gmm",
	     dat,stats.get_means(t+1),stats.get_vars(t+1),stats.get_weights(),
	     targetnames[t]);
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  // plot table summary
  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  plot_table_clusters(htmlfile);
  plot_table_fstats(htmlfile);

  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  // plot clustering
  ///////////////////////////////////////////////////////////////////////////////////////////////////////

//   htmlfile << "<hr>" << endl;
//   htmlfile << "<h3>Hard clustering snapshot</h3><br><br>" << endl;

  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  // save coords
  ///////////////////////////////////////////////////////////////////////////////////////////////////////

  save_coord(dirname);

  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  // create subject-wise reports
  ///////////////////////////////////////////////////////////////////////////////////////////////////////

  for(unsigned int i=0;i<subjnames.size();i++){
    string ihtmlfilename = dirname + "/copain_report_"+num2str(i)+".html";
    ofstream ihtmlfile(ihtmlfilename.c_str());

    ihtmlfile << "<html> " << endl
	      << "<title> COPAIN HTML REPORT </title>"
	      << "<body background=\"file:" << getenv("FSLDIR")
	      << "/doc/images/fsl-bg.jpg\">" << endl
	      << "<hr><center><h1>COPAIN results for " + subjnames[i] + "</h1></center>" << endl
	      << "<hr><p>" << endl;

      // link to group or other subjects
      ihtmlfile << "<center>"<<endl;
      ihtmlfile << "<a href=\"index.html\">Back to group results</a><br><br>"<<endl;
      for(unsigned int s=0;s<subjnames.size();s++)
	ihtmlfile << "<a href=\"copain_report_"+num2str(s)+".html\">"+num2str(s+1)+"</a>   ";

      unsigned int j,jj;
      if(i>0){j=i-1;}else{j=i;}
      if(i<subjnames.size()-1){jj=i+1;}else{jj=i;}
      ihtmlfile << "<br><br><a href=\"copain_report_"+num2str(j)+".html\">&lt;</a>  ";
      ihtmlfile << "<a href=\"copain_report_"+num2str(jj)+".html\">&gt;</a>";
      ihtmlfile << "<br>" << endl;
      ihtmlfile << "</center>" << endl;

      ihtmlfile << "<hr>" << endl;

      ihtmlfile << "<h3>Raw Data</h3><br><br>" << endl;
      // plot histograms for raw data (one image per target)
      Matrix dat(data[i].Nrows(),1);
      for(unsigned int t=0;t<targetnames.size();t++){
	dat.SubMatrix(1,rawdata[i].Nrows(),1,1) = rawdata[i].SubMatrix(1,rawdata[i].Nrows(),t+1,t+1);
	plot_hist(ihtmlfile,dirname,"rawdata"+num2str(i)+"_"+num2str(t+1)+"_hist",dat,targetnames[t]);
      }

      // plot fits
      ihtmlfile << "<hr>" << endl;
      ihtmlfile << "<h3>GMM FIT to preprocessed data</h3><br><br>" << endl;
      ihtmlfile << "Preprocessing consists of log-transforming the data and adding gaussian noise in each dimension (std=0.05) <br><br>" << endl;

      for(unsigned int t=0;t<targetnames.size();t++){
	dat.SubMatrix(1,data[i].Nrows(),1,1) = data[i].SubMatrix(1,data[i].Nrows(),t+1,t+1);
	plot_fit(ihtmlfile,dirname,"data"+num2str(i)+"_"+num2str(t+1)+"_gmm",
		 dat,stats.get_means(t+1),stats.get_vars(t+1),stats.get_weights(),
		 targetnames[t]);
      }

      // plot table
      plot_table_clusters(ihtmlfile,i);

      // plot snapshot of the clustering



  }


}
}
